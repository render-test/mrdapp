const fetch = require('node-fetch');
const express = require('express');

const app = express();
app.get('/', (req, res) =>{
    res.status(200).json({status: 'ok'})
})
app.listen(process.env.PORT || 5000, async () => {
    console.log('App runnin');
});

(async function vote(){
    await fetch('https://mrdapps.com/api/dapp/vote',{method: 'POST',  headers: {'Content-Type': 'application/json'}, body: JSON.stringify({id: 1})});
    await fetch('https://reefaq.io/api/staking/increment-calculation-count',{method: 'POST',  headers: {'Content-Type': 'application/json'}});
    setTimeout(vote, 1000)
})();